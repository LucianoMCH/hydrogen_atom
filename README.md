# hydrogen_atom: Visualización de Orbitales Atómicos y Densidad de Probabilidad

La librería `hydrogen_atom` permite la visualización de orbitales atómicos complejos, imaginarios y reales del átomo de hidrógeno en 3D. También ofrece la capacidad de graficar la densidad de probabilidad asociada con estos orbitales.

## Teoría

### Ecuación de Hamilton para el Átomo de Hidrógeno

La ecuación de Hamilton para el átomo de hidrógeno se expresa como:

$$ H \Psi = E \Psi $$

donde:
- $H$ es el operador Hamiltoniano.
- $\Psi$ es la función de onda.
- $E$ es la energía total.

La forma específica de la ecuación de Hamilton para el átomo de hidrógeno es:

$$ -\frac{\hbar^2}{2\mu} \nabla^2 \psi - \frac{e^2}{4\pi\epsilon_0 r} \psi = E \psi $$

## Solución de la Ecuación

La solución de la ecuación de Hamilton para el átomo de hidrógeno se puede expresar en coordenadas esféricas como:

$$ \psi_{nlm}(r, \theta, \phi) = R_{nl}(r) \cdot Y_{lm}(\theta, \phi) $$

donde:
- $R_{nl}(r)$ es la parte radial.
- $Y_{lm}(\theta, \phi)$ es la parte angular.

Las funciones $R_{nl}(r)$ y $Y_{lm}(\theta, \phi)$ se determinan resolviendo ecuaciones diferenciales específicas asociadas con la ecuación de Hamilton. Para lograr esto, utilizamos funciones especializadas que se encuentran en el módulo `scipy.special`:

- `sph_harm`: Esta función calcula armónicas esféricas, que son cruciales para describir la parte angular de la solución en coordenadas esféricas.
  
- `assoc_laguerre`: Esta función se utiliza para calcular polinomios asociados de Laguerre, necesarios para la parte radial de la solución.

Estas funciones especializadas proporcionan implementaciones eficientes y precisas que simplifican la resolución de las ecuaciones diferenciales asociadas con la ecuación de Hamilton.


## Características Principales y Métodos Relevantes

- **Parámetros Iniciales:**
  - `n` (Número cuántico principal)
  - `l` (Número cuántico orbital)
  - `m` (Número cuántico magnético)

- **Métodos Relevantes:**
    - `angular_graph()`: Grafica la función de onda angular en 3D para la parte compleja, imaginaria y real.

    - `Real_graph()`: Grafica la función de onda real en 3D para todos los valores de \( l \) menores que \( n \).

    - `Density_probability_graph()`: Grafica la densidad de probabilidad en 3D.


## USO

1. **Clona el Repositorio:**
   Clona el repositorio en tu máquina local usando el siguiente comando en tu terminal o línea de comandos:

   ```bash
   git clone https://gitlab.com/LucianoMCH/hydrogen_atom.git
   ```
   
2. **Crea un Entorno Virtual:**
   Crea un entorno virtual en el directorio del repositorio:

   ```bash
   python -m venv venv
   ```

   Esto creará un directorio llamado `venv` que contiene tu entorno virtual.

3. **Activa el Entorno Virtual:**
   Activa el entorno virtual. El proceso puede variar según tu sistema operativo:

   - En Windows:

     ```bash
     .\venv\Scripts\activate
     ```

   - En Linux/Mac:

     ```bash
     source venv/bin/activate
     ```

   Verás el nombre del entorno virtual en tu terminal, indicando que el entorno está activo.

4. **Instala las Dependencias:**
   Instala las dependencias del archivo `requirements.txt`:

   ```bash
   pip install -r requirements.txt
   ```

5. **Ejecuta el Código Principal:**
   Ejecuta tu código principal. Supongamos que tu archivo principal se llama `execution.py`:

   ```bash
   python execution.py
   ```

    Eso es todo! 
    
## Créditos

Esta librería fue desarrollada por Luciano Muñoz.


## Contacto

Si tienes alguna pregunta o comentario, no dudes en ponerte en contacto conmigo a través de mi correo electrónico: [luciano.munoz1@udea.edu.co].

