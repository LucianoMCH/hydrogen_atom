from Hydrogen_Atom import hydrogen_atom

if __name__ == "__main__":
    #Genera la gráfica de la función de onda radial para el estado fundamental del átomo de hidrógeno.
    
    # Define the parameters
    n = 5 # Principal quantum number
    l = 3 # Orbital quantum number
    m = 1 # Magnetic quantum number
    
    #Creamos el objeto
    H = hydrogen_atom(n, l, m)
    
    #Visualiza la función de onda angular en 3D, mostrando los orbitales complejos, imaginarios y reales.
    H.angular_graph()
    
    #Para observar todos los orbitales reales tales que (l < n) utilizando el siguiente método. En este caso, debemos asegurarnos de que el valor de m sea 0.
    #parameters
    n = 5 # Principal quantum number
    l = 3 # Orbital quantum number
    m = 0 # Magnetic quantum number
    
    #Creamos el objeto
    HR = hydrogen_atom(n, l, m)
    HR.Real_graph()
    
    # Para realizar la simulación de las densidades de probabilidad, puedes utilizar el método Density_probability_graph().
    # El método toma un parámetro opcional num_points, que por defecto es 5000. Un número más grande significa más precisión, 
    # pero también un mayor tiempo de ejecución. Puedes ajustar este parámetro según tus necesidades:
    
    num_points = 5000
    H.Density_probability_graph(num_points)
    
    #EL tiempo de ejecución es de alrededor de 3 minutos. 
    # Si quieres que demore menos, puedes reducir el número de puntos a 1000.
    # Aunque esto reducirá la precisión.

    